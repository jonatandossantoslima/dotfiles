"             _
"            (_)
"    __    __ _ _ __ ___  _ __ ___
"    \ \  / /| | '_ ` _ \| '__/ __|
"     \ \/ / | | | | | | | | | (__
"      \__/  |_|_| |_| |_|_|  \___|

" Imports
so ~/dotfiles/Vim/configs/basic.vim
so ~/dotfiles/Vim/configs/plugins.vim
so ~/dotfiles/Vim/configs/coc.vim
so ~/dotfiles/Vim/configs/nerdtree.vim
so ~/dotfiles/Vim/configs/ctrlp.vim
so ~/dotfiles/Vim/configs/fzf.vim
so ~/dotfiles/Vim/configs/rust.vim

" Commands
so ~/dotfiles/Vim/configs/commads.vim

" Functions
so ~/dotfiles/Vim/configs/functions.vim

" Map
so ~/dotfiles/Vim/configs/map.vim

" Spell
so ~/dotfiles/Vim/configs/spell.vim
