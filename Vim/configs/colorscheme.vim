" Background
set background=dark
" set background=light

" Colors
" colors zenbrum
" colors jellybeans
" colors molokai
" colors lucius
" colors gruvbox
" colors embark
" colors badwolf
" colors oceanic-next
" colors paper-color
" colors tender
" colors night-owl
" colors lucario
" colors darcula
" colors apprentice
" colors mango
" colors simpleblack
" colors rigel
" colors enfocado
" colors znake
" colors amber
" colors synthetic
" colors solarized
" colors falcon
colors iceberg
" colors nord
