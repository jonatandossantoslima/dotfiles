" ctrlp

set runtimepath^=~/.vim/bundle/ctrlp.vim
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](doc|tmp|node_modules|.idea|dist|coverage)',
  \ 'file': '\v\.(exe|so|dll|DS_Store)$',
  \ }
let g:ctrlp_show_hidden = 1

