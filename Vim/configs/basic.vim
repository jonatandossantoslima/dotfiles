" Basic configs

execute pathogen#infect()

syntax on
set noshowmode
set expandtab
set ai
set number relativenumber
set hlsearch
set ruler
set list listchars=tab:\ \ ,trail:·
set lcs+=space:·
set autoindent
let mapleader=','
set colorcolumn=80
highlight ColorColumn guibg=red
set confirm
set cursorline
highlight Comment ctermfg=green
filetype plugin indent on
set fileencoding=utf-8
set encoding=utf8
set ffs=unix,dos,mac
set mouse=a
set smarttab
set smartindent
set nowrap
set linebreak
set foldmethod=indent
set nofoldenable
set ttyfast
set backspace=indent,eol,start
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set hidden
set hlsearch
set incsearch
set ignorecase
set smartcase
set autoread
set binary noeol
set noendofline
set nofixendofline
set t_Co=256
set termguicolors

let no_buffers_menu=1
autocmd BufWritePre * %s/\s\+$//e
highlight clear SignColumn

let g:python3_host_prog = '/usr/local/bin/python3'

so ~/dotfiles/Vim/configs/colorscheme.vim
so ~/dotfiles/Vim/configs/polyglot.vim
so ~/dotfiles/Vim/configs/status-line.vim

